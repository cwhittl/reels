jQuery(function($)
{

    $main_reel = $(".main_reel");
    var current_rotation = 0;
    var got_by_deg = 360 / ($main_reel.find("li").length / 2);
    //alert(current_rotation);
    jQuery(".rotate_right").click(function(e)
    {
        e.preventDefault();
        var $this = $(this);
        current_rotation += got_by_deg;
        console.log(current_rotation);
        if (current_rotation > 360)
        {
            current_rotation = 0;
        }
        $main_reel.rotate(current_rotation);
    });
});