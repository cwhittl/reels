<?php
const NBR_ITEMS = 14;
function put_on_circle($circle_size = 300, $item_size = 50 , $rot = 0 ) {
  $half_item =   $item_size / 2;
  $half_parent = $circle_size / 2;
  $top= sin( ($rot) * pi() / 180 ) * $half_parent - $half_item;
  $left= cos( ($rot) * pi() / 180 ) * $half_parent - $half_item;
  return <<<STYLE
    top:  50%;
    left: 50%;
    width:  $item_size;
    height: $item_size;
    margin-top: {$top}px;
    margin-left: {$left}px;
    -webkit-transform: rotate({$rot}deg);
    -moz-transform: rotate({$rot}deg);
    -ms-transform: rotate({$rot}deg);
    -o-transform: rotate({$rot}deg);
    transform: rotate({$rot}deg);
STYLE;
}

?>
<!DOCTYPE html>
<html>
<head>
<meta charset=utf-8 />
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">

<title>Project Reels</title>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.1/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" media="screen" href="style/style.css"/>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.1/js/bootstrap.min.js"></script>
<script src="js/jQueryRotate3.js"></script>
<script src="js/main.js"></script>
</head>

<body>
  <div class="wrapper">

      <a class='rotate_right' href="#">Rotate</a>
      <div class="main_reel">
        <ul>
          <?php
$deg_count = 0;
for ( $i=NBR_ITEMS; $i > 0 ; $i-- ) {
  $angle =  360 / NBR_ITEMS;
  $element_style = put_on_circle( 218, 30, $deg_count );
  echo "<li class='slide_view_{$i}' style='{$element_style}'><div><img src='http://lorempixel.com/30/30'/></div></li>". PHP_EOL;
  $deg_count = $deg_count + $angle;
}
?>
        </ul>
      </div>

  </div>

</body>
</html>
